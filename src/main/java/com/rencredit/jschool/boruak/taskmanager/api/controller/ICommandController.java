package com.rencredit.jschool.boruak.taskmanager.api.controller;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

    void showUnknownCommand(String arg);

    void showInfo();

}
