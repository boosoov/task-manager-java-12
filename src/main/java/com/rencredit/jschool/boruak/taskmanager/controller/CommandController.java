package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.api.controller.ICommandController;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.model.Command;
import com.rencredit.jschool.boruak.taskmanager.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Boruak Sergey");
        System.out.println("E-MAIL: boosoov@gmail.com");
    }

    public void showCommands() {
        System.out.println("[COMMANDS]");
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    public void showUnknownCommand(final String arg) {
        System.out.println("Unknown command: " + arg);
    }

    public void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
